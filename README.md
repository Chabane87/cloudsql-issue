```shell
gcloud projects create mycomp-shared-net-spoke-dev
gcloud compute shared-vpc enable mycomp-shared-net-spoke-dev

gcloud projects create mycomp-shared-net-spoke-prod
gcloud compute shared-vpc enable mycomp-shared-net-spoke-prod

cd network
terraform init
terraform apply
```

```shell
gcloud projects create mycomp-shared-net-spoke-dev
gcloud compute shared-vpc associated-projects add mycomp-myapp-dev --host-project=mycomp-shared-net-spoke-dev

gcloud projects create mycomp-shared-net-spoke-prod
gcloud compute shared-vpc associated-projects add mycomp-myapp-prod --host-project=mycomp-shared-net-spoke-prod

cd cloud-sql
terraform init --backend-config=config/dev/local.backend
terraform apply -var-file=config/dev/terraform.tfvars

terraform init --backend-config=config/prod/local.backend
terraform apply -state=config/prod -var-file=config/prod/terraform.tfvars
```
