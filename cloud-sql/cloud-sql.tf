resource "random_string" "db_name_suffix" {
  length  = 4
  special = false
  upper   = false
}

resource "google_sql_database_instance" "mysql" {
  name             = "mysql-${random_string.db_name_suffix.result}"
  region           = var.cloud_sql_region

  database_version = var.sql_instance_database_version

  settings {
    tier              = var.sql_instance_machine_type
    availability_type = var.sql_instance_availability_type
    disk_size         = var.sql_instance_default_disk_size
    ip_configuration {
      ipv4_enabled        = true
      private_network     = var.network
    }
    backup_configuration {
      binary_log_enabled = true
      enabled = true
      start_time = "08:00"
    }
    location_preference {
      zone = var.mysql_location_preference
    }
  }

  deletion_protection = false

  project = var.project_id
}
