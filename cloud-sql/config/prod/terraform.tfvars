env                      = "prod"
network                  = "projects/mycomp-shared-net-spoke-prod/global/networks/host-network-spoke-prod"
subnetwork               = "projects/mycomp-shared-net-spoke-prod/regions/europe-west1/subnetworks/network-spoke-prod"
project_id               = "mycomp-myapp-prod"
