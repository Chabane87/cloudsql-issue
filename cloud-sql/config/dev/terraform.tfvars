env                      = "dev"
network                  = "projects/mycomp-shared-net-spoke-dev/global/networks/host-network-spoke-dev"
subnetwork               = "projects/mycomp-shared-net-spoke-dev/regions/europe-west1/subnetworks/network-spoke-dev"
project_id               = "mycomp-myapp-dev"
