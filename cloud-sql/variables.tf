variable "network" {
  type = string
}

variable "subnetwork" {
  type = string
}

variable "cloud_sql_region" {
  type = string
  default = "europe-west1"
}

variable "mysql_location_preference" {
  type = string
  default = "europe-west1-b"
}

// deployment project id
variable "project_id" {
  type = string
}

variable "sql_instance_machine_type" {
  type = string
  default = "db-n1-standard-2"
}

variable "sql_instance_database_version" {
  type = string
  default = "MYSQL_8_0"
}

variable "sql_instance_default_disk_size" {
  type = string
  default = "100"
}

variable "sql_instance_availability_type" {
  type = string
  default = "REGIONAL"
}

variable "env" {
  type = string
}
