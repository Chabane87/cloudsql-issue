variable "region" {
  type = string
  default = "europe-west1"
}

variable "envs" {
  type = list(string)
  default = ["dev", "prod"]
}

variable "network_config" {
  type = map(object({
      vpc_spoke_ip_ranges_primary = string
      vpc_spoke_ip_ranges_services = string
      vpc_spoke_ip_ranges_pods = string
  }))

  default = {
    "dev" = {
        vpc_spoke_ip_ranges_primary = "10.10.10.0/24"
        vpc_spoke_ip_ranges_services = "10.10.11.0/24"
        vpc_spoke_ip_ranges_pods = "10.1.0.0/20"
    },
    "prod" = {
        vpc_spoke_ip_ranges_primary = "172.18.0.0/20"
        vpc_spoke_ip_ranges_services = "172.19.10.0/24"
        vpc_spoke_ip_ranges_pods = "172.21.0.0/20"
    }
  }
}

locals {
    nested_config = flatten([
        for env, config in var.network_config : [
            {
                env                         = env
                ip_range_primary            = config.vpc_spoke_ip_ranges_primary
                secondary_ip_range_services = config.vpc_spoke_ip_ranges_services
                secondary_ip_range_pods     = config.vpc_spoke_ip_ranges_pods
            }
        ]
    ])
}

// Spokes prefix
variable "network_spoke_project_id_prefix" {
  type = string
  default = "mycomp-shared-net-spoke"
}
