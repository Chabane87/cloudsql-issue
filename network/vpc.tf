// Spoke
resource "google_compute_network" "network-spoke" {
  count                   = length(var.envs)
  
  name                    = "host-network-spoke-${var.envs[count.index]}"
  auto_create_subnetworks = false
  project                 = "${var.network_spoke_project_id_prefix}-${var.envs[count.index]}"
}

resource "google_compute_subnetwork" "network-spoke" {
  for_each = {
    for ip_range in local.nested_config : "${ip_range.env}" => ip_range
  }

  name                     = "network-spoke-${each.value.env}"
  ip_cidr_range            = each.value.ip_range_primary
  region                   = var.region
  network                  = google_compute_network.network-spoke[index(local.nested_config, each.value)].id
  project                  = "${var.network_spoke_project_id_prefix}-${var.envs[count.index]}"
  secondary_ip_range       = [
    {
        range_name    = "services"
        ip_cidr_range = each.value.secondary_ip_range_services
    },
    {
        range_name    = "pods"
        ip_cidr_range = each.value.secondary_ip_range_pods
    }
  ]
  private_ip_google_access = true
}

resource "google_compute_global_address" "private-ip-peering" {
  count = length(var.envs)

  name = "private-ip-peering-${var.envs[count.index]}"
  purpose = "VPC_PEERING"
  address_type = "INTERNAL"
  prefix_length = 16
  network = google_compute_network.network-spoke[count.index].id
  project = "${var.network_spoke_project_id_prefix}-${var.envs[count.index]}"
}

resource "google_service_networking_connection" "private-vpc-connection" {
  count = length(var.envs)

  network = google_compute_network.network-spoke[count.index].id
  service = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [
    google_compute_global_address.private-ip-peering[count.index].name
  ]
}
