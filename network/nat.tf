resource "google_compute_address" "network-spoke" {
  count   = length(var.envs)

  name    = "manual-ip-host-network-spoke-${var.envs[count.index]}"
  region  = google_compute_subnetwork.network-spoke[var.envs[count.index]].region
  project = "${var.network_spoke_project_id_prefix}-${var.envs[count.index]}"
}

resource "google_compute_router" "network-spoke" {
  count   = length(var.envs)

  name    = "network-spoke-${var.envs[count.index]}"
  region  = google_compute_subnetwork.network-spoke[var.envs[count.index]].region
  network = google_compute_network.network-spoke[count.index].id
  project = "${var.network_spoke_project_id_prefix}-${var.envs[count.index]}"
}

resource "google_compute_router_nat" "network-spoke" {
  count                              = length(var.envs)

  name                               = "network-spoke-${var.envs[count.index]}"
  router                             = google_compute_router.network-spoke[count.index].name
  region                             = google_compute_router.network-spoke[count.index].region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.network-spoke[count.index].self_link]
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  
  project = "${var.network_spoke_project_id_prefix}-${var.envs[count.index]}"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}
